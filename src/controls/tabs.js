/*global $:false */

const template = require('./tabs.nunj');
const _ = require('lodash');

class Tabs {
  constructor(selector, data){
    this.selector = $(selector);
    this.data = data;
    this.handleClick = this.handleClick.bind(this);
    this.buildTabs();
  }
  showContent(type, name){
    $('[data-'+ type +']').removeClass('active');
    $('[data-'+ type +'="' + name + '"]').addClass('active');
  }
  handleClick(e) {
    e.preventDefault();
    const $target = $(e.target);
    const $link = $target.attr('data-command') ? $target : $target.closest('a');
    const command = $link.attr('data-command');
    if (command && !$link.hasClass('active')) {
      if (command === 'change-tab') {
        const tabName = $link.attr('data-tab');
        this.showContent('tab', tabName);
        // show the first item by default
        const id = _.find(this.groupedData.tabs, function(obj){
          return obj.tabName === tabName;
        }).items[0].Id;
        this.showContent('session', id);
      }
      if (command === 'change-session') {
        this.showContent('session', $link.attr('data-session'));
      }
    }
  }
  groupData() {
    // get the unique tracks
    const tabNames = _.uniq(this.data.Items.map(item => item.Track.Title));
    // loop over tab names transform them into objects that hold all items for that tab
    return tabNames.map(tabName => {
      return {
        tabName: tabName,
        items: this.data.Items.filter(function(item){
          return item.Track.Title === tabName;
        })
      };
    });
  }
  buildTabs() {
    this.groupedData = {
      tabs: this.groupData()
    };
    this.selector.append(template(this.groupedData));
    this.showContent('tab', this.groupedData.tabs[0].tabName);
    this.showContent('session', this.groupedData.tabs[0].items[0].Id);
    // attach event delegation
    this.selector.click(this.handleClick);
  }
}

module.exports = Tabs;