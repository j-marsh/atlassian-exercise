/*global $:false */

var Tabs = require('./tabs');
var sessions = require('../data/sessions');

// create a tabs instance and pass it the tab information
new Tabs($('.tabs'), sessions);
