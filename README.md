# An Html/Css/Javascript Coding exercise #

The included files provide a good idea of my usual development process. I've included a bare bones setup that I used while working on this exercise.

The 'public' folder contains the production code, with the html, images, and compiled/minified css and js files. The 'src' folder contains the pre-bundled development code, like my stylus files and js modules.